<footer class="footer-black footer-white">
  <div class="container-fluid">

  </div>
</footer>
</div>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"
  integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
{{-- <script src="https://cdn.jsdelivr.net/npm/chart.js@3.2.1/dist/chart.min.js"></script> --}}

<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>

<script src="https://cdn.datatables.net/1.10.24/js/dataTables.bootstrap4.min.js"></script>

<script src="{{ asset('js/plugins/jquery.validate.js') }}"></script>
<script src="{{ asset('js/plugins/jquery.mask.js') }}"></script>
<script src="{{ asset('js/plugins/jquery.maskmoney.js') }}"></script>
<script src="{{ asset('js/plugins/perfect-scrollbar.js') }}"></script>
<script src="{{ asset('js/plugins/paper-dashboard.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
</body>

</html>
