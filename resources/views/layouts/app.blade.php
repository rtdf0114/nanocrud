@include('elements.header')
    <div id="app">
        <main class="py-4">
            @yield('content')
        </main>
    </div>
@include('elements.footer')
