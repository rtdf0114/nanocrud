@include('elements.header')
@include('elements.sidebar')
@include('elements.navbar')

<div class="row">
  <div class="col-md-12">
    <div class="custom-container">
      <div class="container-header container-lateral">
        <div class="header-box">
          <i class="far fa-folder-open box-icon"></i>
          <h4 class="box-title">Lara Boni</h4>
        </div>
      </div>
      <div class="container-body">
        <div class="row">
          <div class="col-lg col-12 container-box">
            <div class="custom-card">
              <p class="paragraph">Seja bem vindo, neste painel você terá total controle sobre as bonificações.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@include('elements.footer')
