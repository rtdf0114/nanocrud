-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3307
-- Generation Time: Mar 18, 2022 at 12:24 PM
-- Server version: 10.6.5-MariaDB
-- PHP Version: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `boni`
--

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
CREATE TABLE IF NOT EXISTS `employees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_admin` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `login` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `bank_balance` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `id_admin`, `name`, `login`, `password`, `bank_balance`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Maria', 'maria@gmail.com', '$2y$10$etXFf5A9FV1s/5OhtrmrfuhkP17h1cYnAzFGSA.Fa.Wl6Xu0mOfjC', 40, '2022-03-14 01:33:58', '2022-03-14 02:27:44', NULL),
(2, 1, 'André', 'andre@gmail.com', '$2y$10$ZTM49tSbiQXhadkql6RT8O4Pru70qoBU4uX9wwGOOVYuiCQRVC4P2', 100, '2022-03-14 21:24:13', '2022-03-14 21:25:05', NULL),
(3, 1, 'João', 'joao@gmail.com', '$2y$10$aOfAnHhazG/6Fy4eTz4J1OxyskU1Wh8zAv1g0vpvQFbw..fOQ8PhW', 55, '2022-03-17 21:44:09', '2022-03-17 21:45:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `recover_password`
--

DROP TABLE IF EXISTS `recover_password`;
CREATE TABLE IF NOT EXISTS `recover_password` (
  `id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `token` varchar(100) NOT NULL,
  `data` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
CREATE TABLE IF NOT EXISTS `transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('E','S') NOT NULL COMMENT 'E -> entrada, S -> saida',
  `quantity` float(10,2) NOT NULL,
  `note` text NOT NULL,
  `employee_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `type`, `quantity`, `note`, `employee_id`, `admin_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'E', 50.00, 'Entregou o projeto antes do prazo', 1, 1, '2022-03-14 02:24:56', '2022-03-14 02:24:56', NULL),
(2, 'S', 10.00, 'Crédito celular', 1, 1, '2022-03-14 02:27:44', '2022-03-14 02:27:44', NULL),
(3, 'E', 100.00, 'ENTRADA', 2, 1, '2022-03-14 21:25:05', '2022-03-14 21:25:05', NULL),
(4, 'E', 55.00, 'Fez trabalho externo', 3, 1, '2022-03-17 21:45:03', '2022-03-17 21:45:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `type_register`
--

DROP TABLE IF EXISTS `type_register`;
CREATE TABLE IF NOT EXISTS `type_register` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profile` int(11) NOT NULL,
  `description` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `type_register`
--

INSERT INTO `type_register` (`id`, `profile`, `description`) VALUES
(1, 1, 'Administrador');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `profile` int(11) NOT NULL,
  `email` varchar(250) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `profile`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '', 1, 'admin@gmail.com', NULL, '$2y$10$z2K/us3C0YBLKYGRi2Uc6O9b.VMfPNSfU5/XikbOuJo21jV5cAW8G', NULL, NULL, NULL, NULL),
(2, 'João', 1, 'joao@gmail.com', NULL, '$2y$10$anM3kM82E4Co9FGByNn8zuxOn0K4z57gM6QHAUjDejUaYd1/ZAH3C', NULL, '2022-03-14 03:38:39', '2022-03-14 03:38:39', NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
